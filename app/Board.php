<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = [
      'number',
      'name',
      'hse',
      'hse_status',
      'mant',
      'mant_status',
      'ing',
      'ing_status',
      'cal',
      'cal_status',
      'serv',
      'serv_status',

  ];
}
