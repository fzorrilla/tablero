<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive_public extends Model
{
      protected $fillable = [
      'id',
      'role_id',
      'epps',
      'epps_status',
      'electric_system',
      'electric_system_status',
      'limit',
      'limit_status',
      'other',
      'other_status',
  ];
}
