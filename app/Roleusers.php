<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roleusers extends Model
{
	protected $fillable = [
	    'role_id',
	    'user_id',
	    'create_user',
	    'edit_user',
	    'delete_user',
	    'permission_user',
	    'create_board',
	    'edit_board',
	    'delete_board',
	    'permission_board',
	    'bulk_load',
	    'hse_board',
	    'ing_board',
	    'mant_board',
	    'cal_board',
	    'serv_board',
	];
}
