<?php

namespace App\Http\Controllers;
use App\Board;
use Illuminate\Http\Request;

class ReportController extends Controller
{
     public function index()
    {
        $boards = Board::all();
        //dd($boards);
        return view('report', compact('boards'));


    }
}
