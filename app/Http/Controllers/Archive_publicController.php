<?php

namespace App\Http\Controllers;
use App\Archive_public;
use Illuminate\Http\Request;

class Archive_publicController extends Controller
{
    public function index()
    {
        $archives = Archive_public::all();

        return view('index_archive', compact('archives'));
    }
     public function create()
    {
         return view('edit_archive');

    }
    public function edit($id)
    {
        $archive = Archive_public::where('id', $id)->get();
         return view('edit_archive');
    }
     public function update(Request $request, $id)
    {
        //dd($request->all());
        $archives = Archive_public::find($id);
        $archives->id = $request->id;
        $archives->role_id = $request->role_id;
        if($request->type_area == 'epps')
        {
           $archives->epps = $request->archive;
        }
        if($request->type_area == 'electric_system')
        {
           $archives->electric_system = $request->archive;
        }
         if($request->type_area == 'limit')
        {
           $archives->limit = $request->archive;
        }
        if($request->type_area == 'other')
        {
           $archives->other = $request->archive;
        }
        $archives->save();
         return redirect()->route('index_archive')->withStatus(__('Actualizado Archivo Público con éxito.'));
    }
}
