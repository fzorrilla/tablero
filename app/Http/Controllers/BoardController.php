<?php

namespace App\Http\Controllers;
use App\Board;
use App\Roleusers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class BoardController extends Controller
{
     public function index()
    {
        $boards = Board::all();
        return view('index_board', compact('boards'));


    }
     public function create()
    {
       // $roles = Rol::all();
        //return view('users.create', compact('roles'));
         return view('create_board');

    }

    public function edit($id)
    {
        $boards = Board::where('id', $id)->get();
        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
          }
         return view('edit_board', compact('id','number','name','boards'));
    }
       public function edit_public($id)
    {
        $boards = Board::where('id', $id)->get();
        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
          }
         return view('edit_board_public', compact('id','number','name','boards'));
    }
      public function edit_superv($user_id,$id_board)
    {

        $boards = Board::where('id', $id_board)->get();
        //dd($boards);
        $type_boards = Roleusers::where('user_id', $user_id)->get();

        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
          }
        foreach ($type_boards as $key => $value) {
            $hse = $value->hse_board;
            $ing = $value->ing_board;
            $mant = $value->mant_board;
            $cal = $value->cal_board;
            $serv = $value->serv_board;
          }

         return view('edit_board', compact('id','number','name','boards','hse','ing','mant','cal','serv'));
    }
     public function qr($id)
    {
       // dd($id);
        $boards = Board::where('id', $id)->get();
        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
          }

         return view('qr_board', compact('number'));
    }
    public function store(Request $request)
    {
    //  dd($request->id);
        $board = new Board;
        $board->number = $request->number;
        $board->name = $request->name;
        $board->save();
        $number = $board->number;
         //return redirect()->route('index_board')->withStatus(__('Tablero creado con éxito.'));
        return view('qr_board', compact('number'));
    }

     public function update(Request $request, $id)
    {
        //dd($request->all());
        if ($request->file('archive') == null) {
            $ruta = "";
        }else{
           $ruta = $request->file('archive')->store('public');
        $board = Board::find($id);
       // $pdf_b64 = base64_decode($request->archive);
        $board->number = $request->number;
        $board->name = $request->name;
        if($request->type_area == 'epps')
        {
           $board->epps = $ruta;
           $board->epps_status = $request->status;
        }
        if($request->type_area == 'electric_system')
        {
           $board->electric_system = $ruta;
           $board->electric_system_status = $request->status;
        }
         if($request->type_area == 'limit')
        {
           $board->limit = $ruta;
           $board->limit_status = $request->status;
        }
         if($request->type_area == 'other')
        {
           $board->other = $ruta;
           $board->other_status = $request->status;
        }
        if($request->type_area == 'hse')
        {
           $board->hse = $ruta;
           $board->hse_status = $request->status;
        }
        if($request->type_area == 'mant')
        {
           $board->mant = $ruta;
            $board->mant_status = $request->status;
        }
         if($request->type_area == 'ing')
        {
           $board->ing = $ruta;
            $board->ing_status = $request->status;
        }
        if($request->type_area == 'cal')
        {
           $board->cal = $ruta;
            $board->cal_status = $request->status;
        }
        if($request->type_area == 'serv')
        {
           $board->serv = $ruta;
            $board->serv_status = $request->status;
        }
        $board->save();
        if($request->role_id == 3)
        {
        return redirect()->route('edit_superv_board',[$request->user_id,$id])->withStatus(__('Tablero actualizado con éxito.'));
        }
        if($request->type_area == 'epps' or $request->type_area == 'electric_system' or $request->type_area == 'limit' or $request->type_area == 'other')
        {
           return redirect()->route('edit_board_public',[$id])->withStatus(__('Tablero actualizado con éxito.'));
        }

        return redirect()->route('edit_board',[$id])->withStatus(__('Tablero actualizado con éxito.'));

        }

    }
    public function destroy(Request $request, $id)
    {
       /// $delet = Board::find($id);
      //  $id->delete();

    //$id = $request->input('id');
    Board::find($id)->delete();

        return redirect()->route('index_board')->withStatus(__('Tablero eliminado con éxito.'));
    }
    /////////////////////////////////////////////////////////////////////////////////
    //MOBILE
    public function index_mobile($id)
    {
        //$boards = Board::all();
        return view('mobile.mobile_login', compact('id'));
    }
    public function buscar_qr($id)
    {
       // dd($id);
        $boards = Board::where('id', '1')->get();
        foreach ($boards as $key => $value) {
            $id = $value->id;
            $number = $value->number;
            $name = $value->name;
            $epps = $value->epps;
            $electric_system = $value->electric_system;
            $limit = $value->limit;
            $other = $value->other;
            $ing = $value->ing;
            $hse = $value->hse;
            $mant = $value->mant;
            $cal = $value->cal;
            $serv = $value->serv;
          }

         return view('mobile.mobile_tablero', compact('number','name','epps','electric_system','limit','other','ing','hse','mant','cal','serv','id'));
    }
}
