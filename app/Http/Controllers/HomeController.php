<?php

namespace App\Http\Controllers;
use App\User;
use App\Rol;
use App\Board;
use App\Roleusers;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = User::count();
        $user_sa = User::where('role_id', 1)->get();
        $user_total_sa =count($user_sa);
        $user_a = User::where('role_id', 2)->get();
        $user_total_a =count($user_a);
        $user_s = User::where('role_id', 3)->get();
        $user_total_s =count($user_s);
        $board = Board::count();
        $archive_epps = Board::count('epps');
        $archive_electric_system = Board::count('electric_system');
        $archive_limit = Board::count('limit');
        $archive_other = Board::count('other');
        $archive_hse = Board::count('hse');
        $archive_mant = Board::count('mant');
        $archive_ing = Board::count('ing');
        $archive_cal = Board::count('cal');
        $archive_serv = Board::count('serv');
        $archive = 0;
        $archive = $archive_epps + $archive_electric_system + $archive_limit + $archive_other + $archive_hse + $archive_mant + $archive_ing + $archive_cal + $archive_serv;
        $archive_off = 0;
        $archive_off = ($board * 9)-$archive;
        return view('dashboard', compact('user','board','archive','archive_off','user_total_sa','user_total_a','user_total_s'));
    }
}
