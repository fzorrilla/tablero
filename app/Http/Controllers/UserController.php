<?php

namespace App\Http\Controllers;

use App\User;
use App\Rol;
use App\Roleusers;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    public function permissions($id)
    {
        //dd($id);

         $roles = Roleusers::where('user_id', $id)->get();
         $users = User::where('id', $id)->get();
      //   $rol_id = Rol::where('id', $id)->get();
        // dd($rol_id);
         foreach ($roles as $key => $value) {
            $id = $value->id;
            $role_id = $value->role_id;
          }
        foreach ($users as $key => $value) {
            $obtain = $value->id;
          }
      /* foreach ($rol_id as $key => $value) {
            $obtain_rol = $value->id;
          }*/
       // dd($obtain);
        return view('users.permissions', compact('id','roles','obtain','users','role_id'));
    }
     public function update_permissions(Request $request, $id)
    {
        //dd($request->all());
        $rolemodel = Roleusers::find($id);
        $rolemodel->create_user = $request->create_user;
        $rolemodel->edit_user = $request->edit_user;
        $rolemodel->delete_user = $request->delete_user;
        $rolemodel->permission_user = $request->permission_user;
        $rolemodel->create_board = $request->create_board;
        $rolemodel->edit_board = $request->edit_board;
        $rolemodel->delete_board = $request->delete_board;
        $rolemodel->permission_board = $request->permission_board;
        $rolemodel->bulk_load = $request->bulk_load;
        $rolemodel->hse_board = $request->hse_board;
        $rolemodel->ing_board = $request->ing_board;
        $rolemodel->mant_board = $request->mant_board;
        $rolemodel->cal_board = $request->cal_board;
        $rolemodel->serv_board = $request->serv_board;
        $rolemodel->save();

        return redirect()->route('user.index')->withStatus(__('Permisos actualizado con éxito.'));
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $roles = Rol::all();
        return view('users.create', compact('roles'));

    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $model)
    {
        $user = new User;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $request->role_id;
        $user->save();
        //dd($user->id);
        $obt = Roleusers::create([
            'role_id' =>$request->role_id,
            'user_id' =>$user->id,
            'create_user' =>'0',
            'edit_user'=>'0',
            'delete_user'=>'0',
            'permission_user'=>'0',
            'create_board'=>'0',
            'edit_board'=>'0',
            'delete_board'=>'0',
            'permission_board'=>'0',
            'bulk_load'=>'0',
            'hse_board'=>'0',
            'ing_board'=>'0',
            'mant_board'=>'0',
            'cal_board'=>'0',
            'serv_board'=>'0'
            ]);

        return redirect()->route('user.index')->withStatus(__('Usuario creado con éxito.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User  $user)
    {
         //dd($request->all());
        $user->update([
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        /*$user->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except([$request->get('password') ? '' : 'password']
        ));

        $obt = Roleusers::create([
              'role_id' =>$request->role_id,
              'user_id' =>$user->id
            ]);*/
        return redirect()->route('user.index')->withStatus(__('Usuario actualizado con éxito.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        $user->delete();

        return redirect()->route('user.index')->withStatus(__('Usuario eliminado con éxito.'));
    }
}
