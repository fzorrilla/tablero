<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('report', 'ReportController@index')->name('report');
//Permisos de Usuarios
Route::get('/permissions/{id}', 'UserController@permissions')->name('user.permissions');
Route::put('/permissions/{id}', 'UserController@update_permissions')
    ->name('user.update_permissions');
//Fin de Usuarios
//tableros
Route::get('index_board', 'BoardController@index')->name('index_board');
Route::get('create_board', 'BoardController@create')->name('create_board');
Route::post('create_board', 'BoardController@store')->name('board.store');
Route::get('/edit_board/{id}', 'BoardController@edit')->name('edit_board');
Route::get('/edit_board_public/{id}', 'BoardController@edit_public')->name('edit_board_public');
Route::get('/edit_superv_board/{user_id},{board_id}', 'BoardController@edit_superv')->name('edit_superv_board');
Route::put('/edit_board/{id}', 'BoardController@update')
    ->name('board.update');
Route::delete('/index_board/{id}', 'BoardController@destroy')->name('destroy_board');
Route::get('/qr_board/{id}', 'BoardController@qr')->name('qr_board');
//Archive
Route::get('index_archive', 'Archive_publicController@index')->name('index_archive');
//Route::post('/edit_archive/{id}', 'Archive_publicController@edit')->name('edit_archive');
Route::get('edit_archive', 'Archive_publicController@create')->name('edit_archive');
Route::put('/edit_archive/{id}', 'Archive_publicController@update')->name('update_archive');
//fin archive
//fin tableros
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

