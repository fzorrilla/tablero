@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Gestión de Tableros')])
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link href = "https://fonts.googleapis.com/css?family= Roboto & display = swap" rel = "stylesheet">
<style type="text/css">
/* ----- ----- ----- ----- */
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0; }

body {
  font-size: 16px;
  background: #fff; }

a {
  color: #FF4136; }

.wrap {
  width: 90%;
  max-width: 1000px;
  margin: auto; }

.info {
  text-align: center;
  padding: 20px;
  color: #001F3F;
  border-bottom: 1px solid #ccc; }
  .info p {
    margin-top: 20px; }

.formulario {
  /* --------------------------------------- */
  /* ----- Radio Button */
  /* --------------------------------------- */
  /* --------------------------------------- */
  /* ----- Checkbox */
  /* --------------------------------------- */ }
  .formulario h2 {
    font-size: 16px;
    color: #001F3F;
    margin-bottom: 20px;
    margin-left: 20px; }
  .formulario > div {
    padding: 20px 0;
    border-bottom: 1px solid #ccc; }
  .formulario .radio label,
  .formulario .checkbox label {
    display: inline-block;
    cursor: pointer;
    color: #8e24aa;
    position: relative;
    padding: 5px 15px 5px 51px;
    font-size: 1em;
    border-radius: 5px;
    -webkit-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease; }
    .formulario .radio label:hover,
    .formulario .checkbox label:hover {
      background: rgba(255, 65, 54, 0.1); }
    .formulario .radio label:before,
    .formulario .checkbox label:before {
      content: "";
      display: inline-block;
      width: 17px;
      height: 17px;
      position: absolute;
      left: 15px;
      border-radius: 50%;
      background: none;
      border: 3px solid #8e24aa; }
  .formulario input[type="radio"] {
    display: none; }
    .formulario input[type="radio"]:checked + label:before {
      display: none; }
    .formulario input[type="radio"]:checked + label {
      padding: 5px 15px;
      background: #8e24aa;
      border-radius: 2px;
      color: #fff; }
  .formulario .checkbox label:before {
    border-radius: 3px; }
  .formulario .checkbox input[type="checkbox"] {
    display: none; }
    .formulario .checkbox input[type="checkbox"]:checked + label:before {
      display: none; }
    .formulario .checkbox input[type="checkbox"]:checked + label {
      background: #8e24aa;
      color: #fff;
      padding: 5px 15px; }
</style>
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('board.update', $id) }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Agregar Tablero Públicos') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{route('index_board')}}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                  </div>
                </div>
                 @if (Auth::user()->role_id == 1)
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <input type="hidden" name="number" value="{{$number}}">
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number', $number) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name', $name) }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                     <option value="epps">EPPs</option>
                     <option value="electric_system">Sistema electricos</option>
                     <option value="limit">Limites</option>
                     <option value="other">Otro</option>
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                      <input id="file" class="none" type="file" name="archive" />
                    </label>
                  </div>
                  <div class="row">
                  <div class="col-sm-12">
                    <input type="radio" name="status" id="publico" value="1" checked="">
                          <label for="publico">Público</label>
                    <input type="radio" name="status" id="privado" value="0">
                          <label for="privado">Privado</label>
                  </div>
                </div>
              </div><!--fin-->
              @endif
                @if (Auth::user()->role_id == 2)
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <input type="hidden" name="number" value="{{$number}}">
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number', $number) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <input type="hidden" name="name" value="{{$name}}">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name', $name) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                      <option value="epps">EPPs</option>
                     <option value="electric_system">Sistema electricos</option>
                     <option value="limit">Limites</option>
                     <option value="other">Otro</option>
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                       <input id="file" class="none" type="file" name="archive" />
                    </label>
                  </div>
                        <div class="row">
                  <div class="col-sm-12">
                    <input type="radio" name="status" id="publico" value="1" checked="">
                          <label for="publico">Público</label>
                    <input type="radio" name="status" id="privado" value="0">
                          <label for="privado">Privado</label>
                  </div>
                </div>
              </div><!--fin-->
              @endif
               @if (Auth::user()->role_id == 3)
               <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
               <input type="hidden" name="role_id" value="{{Auth::user()->role_id}}">
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <input type="hidden" name="number" value="{{$number}}">
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number',$number) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <input type="hidden" name="name" value="{{$name}}">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name',$name ) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                    @if($epps == 1)
                     <option value="epps">EPPs</option>
                    @endif
                    @if($electric_system == 1)
                     <option value="electric_system">Sistema Eléctrico</option>
                    @endif
                    @if($limit == 1)
                     <option value="limit">Limites</option>
                    @endif
                    @if($other == 1)
                     <option value="other">Otro</option>
                    @endif
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                       <input id="file" class="none" type="file" name="archive" />
                    </label>
                  </div>
                        <div class="row">
                  <div class="col-sm-12">
                    <input type="radio" name="status" id="publico" value="1" checked="">
                          <label for="publico">Público</label>
                    <input type="radio" name="status" id="privado" value="0">
                          <label for="privado">Privado</label>
                  </div>
                </div>
              </div><!--fin-->
              @endif

              </div><!--fin-->
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Actualizar Tablero') }}</button>
              </div>
            </div>

          </form>
          <div class="card-header card-header-primary ">
              <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                          <center><b>{{ __('Archivo') }}</b></center>
                      </th>
                       <th>
                          <b>{{ __('Adjunto') }}</b>
                      </th>
                    </thead>
                    <tbody>
                       @foreach($boards as $board)
                      <tr>
                      <td>
                         <center>EEPs</center>
                      </td>
                      <td>
                        @if($board->epps <> '')
                         <a href="#" value="{{ $board->epps }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                     <tr>
                      <td>
                         <center>Sistema Eléctrico</center>
                      </td>
                      <td >
                         @if($board->electric_system <> '')
                         <a href="#" value="{{ $board->electric_system }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>

                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                      <tr>
                        <tr>
                       <td>
                         <center>Limites</center>
                      </td>
                      <td>
                         @if($board->limit <> '')
                         <a href="#" value="{{ $board->limit }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                     <tr>
                       <td>
                         <center>Otros</center>
                      </td>
                      <td>
                         @if($board->other <> '')
                         <a href="#" value="{{ $board->other }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection