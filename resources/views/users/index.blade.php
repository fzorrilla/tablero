@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Gestión de usuarios')])
 <script type="text/javascript">
    (function(document) {
      'use strict';

      var LightTableFilter = (function(Arr) {

        var _input;

        function _onInputEvent(e) {
          _input = e.target;
          var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
          Arr.forEach.call(tables, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
              Arr.forEach.call(tbody.rows, _filter);
            });
          });
        }

        function _filter(row) {
          var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            Arr.forEach.call(inputs, function(input) {
              input.oninput = _onInputEvent;
            });
          }
        };
      })(Array.prototype);

      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          LightTableFilter.init();
        }
      });

    })(document);
    </script>
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Usuarios') }}</h4>
                <p class="card-category"> {{ __('Aquí puedes administrar usuarios') }}</p>
              </div>
              <br>
               <div class="row">
                  <div class="col-12 text-right">
                    <input class="form-control col-md-3 light-table-filter" style="display: inline;" data-table="order-table" type="text" placeholder="Buscar" title="Presione Esc para supender la búsqueda">
                  </div>
                </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">Cerrar</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{ route('user.create') }}" class="btn btn-sm btn-primary">{{ __('Agregar Usuario') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table  order-table">
                     @if (Auth::user()->role_id == 1)
                    <thead class="text-primary">
                      <th>
                          {{ __('Nombre') }}
                      </th>
                       <th>
                          {{ __('Apellidos') }}
                      </th>
                      <th>
                        {{ __('Correo Electronico') }}
                      </th>
                      <th>
                        {{ __('Permisos') }}
                      </th>
                      <th class="text-right">
                        {{ __('Acción') }}
                      </th>
                    </thead>
                    <tbody id="user">
                      @foreach($users as $user)
                        <tr>
                          <td>
                            {{ $user->name }}
                          </td>
                          <td>
                            {{ $user->surname }}
                          </td>
                          <td>
                            {{ $user->email }}
                          </td>
                          <td>
                            @if($user->id == 1)

                            @else
                               <a rel="tooltip" class="btn btn-warning btn-link" href="{{ route('user.permissions', $user) }}" data-original-title="" title="">
                                <i class="material-icons">vpn_key</i>
                                <div class="ripple-container"></div>
                              </a>
                              @endif
                          </td>

                          <td class="td-actions text-right">
                              <form action="{{ route('user.destroy', $user) }}" method="post">
                                  @csrf
                                  @method('delete')
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('user.edit', $user) }}" data-original-title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="Eliminar Usuario" onclick="confirm('{{ __("Estás seguro de que deseas eliminar a este usuario?") }}') ? this.parentElement.submit() : ''">
                                      <i class="material-icons">delete</i>
                                      <div class="ripple-container"></div>
                                  </button>
                              </form>
                          </td>
                        </tr>
                      @endforeach
                      @endif
                      @if (Auth::user()->role_id == 2)
                    <thead class=" text-primary">
                      <th>
                          {{ __('Nombre') }}
                      </th>
                       <th>
                          {{ __('Apellidos') }}
                      </th>
                      <th>
                        {{ __('Correo Electronico') }}
                      </th>
                      <th>
                        {{ __('Permisos') }}
                      </th>
                      <th class="text-right">
                        {{ __('Acción') }}
                      </th>
                    </thead>
                    <tbody id="user">
                      @foreach($users as $user)
                        <tr>
                        @if($user->role_id ==2 or $user->role_id==3)
                          <td>
                            {{ $user->name }}
                          </td>
                          <td>
                            {{ $user->surname }}
                          </td>
                          <td>
                            {{ $user->email }}
                          </td>
                          <td>
                            {{-- $user->created_at->format('Y-m-d') --}}
                                <a rel="tooltip" class="btn btn-warning btn-link" href="{{ route('user.permissions', $user) }}" data-original-title="" title="">
                                <i class="material-icons">vpn_key</i>
                                <div class="ripple-container"></div>
                              </a>
                          </td>

                          <td class="td-actions text-right">
                              <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('profile.edit') }}" data-original-title="" >
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </a>
                          </td>
                          @endif
                        </tr>
                      @endforeach
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  @endsection