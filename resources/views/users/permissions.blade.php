@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Gestión de usuarios')])

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<link href = "https://fonts.googleapis.com/css?family= Roboto & display = swap" rel = "stylesheet">
<style type="text/css">
/* ----- ----- ----- ----- */
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  margin: 0;
  padding: 0; }

body {
  font-size: 16px;
  background: #fff; }

a {
  color: #FF4136; }

.wrap {
  width: 90%;
  max-width: 1000px;
  margin: auto; }

.info {
  text-align: center;
  padding: 20px;
  color: #001F3F;
  border-bottom: 1px solid #ccc; }
  .info p {
    margin-top: 20px; }

.formulario {
  /* --------------------------------------- */
  /* ----- Radio Button */
  /* --------------------------------------- */
  /* --------------------------------------- */
  /* ----- Checkbox */
  /* --------------------------------------- */ }
  .formulario h2 {
    font-size: 16px;
    color: #001F3F;
    margin-bottom: 20px;
    margin-left: 20px; }
  .formulario > div {
    padding: 20px 0;
    border-bottom: 1px solid #ccc; }
  .formulario .radio label,
  .formulario .checkbox label {
    display: inline-block;
    cursor: pointer;
    color: #8e24aa;
    position: relative;
    padding: 5px 15px 5px 51px;
    font-size: 1em;
    border-radius: 5px;
    -webkit-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease; }
    .formulario .radio label:hover,
    .formulario .checkbox label:hover {
      background: rgba(255, 65, 54, 0.1); }
    .formulario .radio label:before,
    .formulario .checkbox label:before {
      content: "";
      display: inline-block;
      width: 17px;
      height: 17px;
      position: absolute;
      left: 15px;
      border-radius: 50%;
      background: none;
      border: 3px solid #8e24aa; }
  .formulario input[type="radio"] {
    display: none; }
    .formulario input[type="radio"]:checked + label:before {
      display: none; }
    .formulario input[type="radio"]:checked + label {
      padding: 5px 15px;
      background: #8e24aa;
      border-radius: 2px;
      color: #fff; }
  .formulario .checkbox label:before {
    border-radius: 3px; }
  .formulario .checkbox input[type="checkbox"] {
    display: none; }
    .formulario .checkbox input[type="checkbox"]:checked + label:before {
      display: none; }
    .formulario .checkbox input[type="checkbox"]:checked + label {
      background: #8e24aa;
      color: #fff;
      padding: 5px 15px; }
</style>

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
           <form method="post" action="{{ route('user.update_permissions', $id) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')
            <input type="hidden" name="user_id" value="{{$obtain}}">
            <input type="hidden" name="role_id" value="{{--$role_id--}}">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Permisos') }}</h4>
                 @foreach($users as $user)
                <b><h4 class="card-category"> {{ __(' '.$user->name.'  '.$user->surname)}}</h4></b>
                @endforeach
              </div>

              <div class="card-body">
                  <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('user.index') }}" class="btn btn-sm btn-primary">{{ __('Volver a la Lista') }}</a>
                  </div>
                    @if (Auth::user()->role_id == 1)
                <div class="table-responsive formulario">
                  <table class="table">
                     @foreach($roles as $rol)
                    <div class="radio">
                         <h4><b><p>Usuarios</p></b></h4>
                       <div class="row" align="center">
                        <!--USuario -->
                         <div class="col-md-3">
                         <h2><b>Crear Usuario</b></h2>
                          <input type="radio" name="create_user" id="create_si" value="1" {{ ($rol->create_user=="1")? "checked" : "" }}>
                          <label for="create_si">Si</label>
                          <input type="radio" name="create_user" id="create_no" value="0" {{ ($rol->create_user=="0")? "checked" : "" }}>
                          <label for="create_no">No</label>
                        </div>
                        <div class="col-md-3">
                         <h2><b>Editar Usuario</b></h2>
                        <input type="radio" name="edit_user" id="edit_si" value="1" {{ ($rol->edit_user=="1")? "checked" : "" }}>
                        <label for="edit_si">Si</label>
                        <input type="radio" name="edit_user" id="edit_no" value="0" {{ ($rol->edit_user=="0")? "checked" : "" }}>
                        <label for="edit_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Eliminar Usuario</b></h2>
                        <input type="radio" name="delete_user" id="delete_si" value="1" {{ ($rol->delete_user=="1")? "checked" : "" }}>
                        <label for="delete_si">Si</label>
                        <input type="radio" name="delete_user" id="delete_no" value="0" {{ ($rol->delete_user=="0")? "checked" : "" }}>
                        <label for="delete_no">No</label>
                      </div>
                      <div class="col-md-3">
                          <h2><b>Asignar Permisos</b></h2>
                        <input type="radio" name="permission_user" id="per_si" value="1" {{ ($rol->permission_user=="1")? "checked" : "" }}>
                        <label for="per_si">Si</label>
                        <input type="radio" name="permission_user" id="per_no" value="0" {{ ($rol->permission_user=="0")? "checked" : "" }}>
                        <label for="per_no">No</label>
                      </div>
                       </div>
                       <br>
                       <h4><b><p>Tableros</p></b></h4>
                       <!-- Tablero-->
                       <div class="row" align="center">
                         <div class="col-md-3">
                         <h2><b>Crear Tablero</b></h2>
                          <input type="radio" name="create_board" id="create_board_si" value="1" {{ ($rol->create_board=="1")? "checked" : "" }}>
                          <label for="create_board_si">Si</label>
                          <input type="radio" name="create_board" id="create_board_no" value="0" {{ ($rol->create_board=="0")? "checked" : "" }}>
                          <label for="create_board_no">No</label>
                        </div>
                        <div class="col-md-3">
                         <h2><b>Editar Tablero</b></h2>
                        <input type="radio" name="edit_board" id="edit_board_si" value="1" {{ ($rol->edit_board=="1")? "checked" : "" }}>
                        <label for="edit_board_si">Si</label>
                        <input type="radio" name="edit_board" id="edit_board_no" value="0" {{ ($rol->edit_board=="0")? "checked" : "" }}>
                        <label for="edit_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Eliminar Tablero</b></h2>
                        <input type="radio" name="delete_board" id="delete_board_si" value="1" {{ ($rol->delete_board=="1")? "checked" : "" }}>
                        <label for="delete_board_si">Si</label>
                        <input type="radio" name="delete_board" id="delete_board_no" value="0"  {{ ($rol->delete_board=="0")? "checked" : "" }}>
                        <label for="delete_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Asignar Permisos</b></h2>
                        <input type="radio" name="permission_board" id="permission_board_si" value="1" {{ ($rol->permission_board=="1")? "checked" : "" }}>
                        <label for="permission_board_si">Si</label>
                        <input type="radio" name="permission_board" id="permission_board_no" value="0" {{ ($rol->permission_board=="0")? "checked" : "" }}>
                        <label for="permission_board_no">No</label>
                      </div>
                       </div>
                        <br>
                       <h4><b><p>Archivos</p></b></h4>
                       <!-- archivos-->
                       <div class="row" align="center">
                         <div class="col-md-3">
                         <h2><b>Carga Masiva</b></h2>
                          <input type="radio" name="bulk_load" id="bulk_load_si" value="1" {{ ($rol->bulk_load=="1")? "checked" : "" }}>
                          <label for="bulk_load_si">Si</label>
                          <input type="radio" name="bulk_load" id="bulk_load_no" value="0" {{ ($rol->bulk_load=="0")? "checked" : "" }}>
                          <label for="bulk_load_no">No</label>
                        </div>
                        <div class="col-md-3">
                         <h2><b>HSE</b></h2>
                        <input type="radio" name="hse_board" id="hse_board_si" value="1" {{ ($rol->hse_board=="1")? "checked" : "" }}>
                        <label for="hse_board_si">Si</label>
                        <input type="radio" name="hse_board" id="hse_board_no" value="0" {{ ($rol->hse_board=="0")? "checked" : "" }}>
                        <label for="hse_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Ingenieria</b></h2>
                        <input type="radio" name="ing_board" id="ing_board_si" value="1" {{ ($rol->ing_board=="1")? "checked" : "" }}>
                        <label for="ing_board_si">Si</label>
                        <input type="radio" name="ing_board" id="ing_board_no" value="0" {{ ($rol->ing_board=="0")? "checked" : "" }}>
                        <label for="ing_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Mantenimiento</b></h2>
                        <input type="radio" name="mant_board" id="mant_board_si" value="1" {{ ($rol->mant_board=="1")? "checked" : "" }}>
                        <label for="mant_board_si">Si</label>
                        <input type="radio" name="mant_board" id="mant_board_no" value="0" {{ ($rol->mant_board=="0")? "checked" : "" }}>
                        <label for="mant_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Calidad</b></h2>
                        <input type="radio" name="cal_board" id="cal_board_si" value="1" {{ ($rol->cal_board=="1")? "checked" : "" }}>
                        <label for="cal_board_si">Si</label>
                        <input type="radio" name="cal_board" id="cal_board_no" value="0" {{ ($rol->cal_board=="0")? "checked" : "" }}>
                        <label for="cal_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Servicio</b></h2>
                        <input type="radio" name="serv_board" id="serv_board_si" value="1" {{ ($rol->serv_board=="1")? "checked" : "" }}>
                        <label for="serv_board_si">Si</label>
                        <input type="radio" name="serv_board" id="serv_board_no" value="0" {{ ($rol->serv_board=="0")? "checked" : "" }}>
                        <label for="serv_board_no">No</label>
                      </div>
                       </div>
                  </div>
                  @endforeach
                  </table>

                </div>
                @endif
                  @if (Auth::user()->role_id == 2)
                   <div class="table-responsive formulario">
                  <table class="table">
                     @foreach($roles as $rol)
                    <div class="radio">
                       <h4><b><p>Archivos</p></b></h4>
                       <!-- archivos-->
                       <input type="hidden" name="create_user" value="0">
                       <input type="hidden" name="edit_user" value="0">
                       <input type="hidden" name="delete_user" value="0">
                       <input type="hidden" name="permission_user" value="0">
                       <input type="hidden" name="create_board" value="0">
                       <input type="hidden" name="edit_board" value="0">
                       <input type="hidden" name="delete_board" value="0">
                       <input type="hidden" name="permission_board" value="0">
                       <input type="hidden" name="bulk_load" value="0">
                       <div class="row" align="center">
                        <div class="col-md-3">
                         <h2><b>HSE</b></h2>
                        <input type="radio" name="hse_board" id="hse_board_si" value="1" {{ ($rol->hse_board=="1")? "checked" : "" }}>
                        <label for="hse_board_si">Si</label>
                        <input type="radio" name="hse_board" id="hse_board_no" value="0" {{ ($rol->hse_board=="0")? "checked" : "" }}>
                        <label for="hse_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Ingenieria</b></h2>
                        <input type="radio" name="ing_board" id="ing_board_si" value="1" {{ ($rol->ing_board=="1")? "checked" : "" }}>
                        <label for="ing_board_si">Si</label>
                        <input type="radio" name="ing_board" id="ing_board_no" value="0" {{ ($rol->ing_board=="0")? "checked" : "" }}>
                        <label for="ing_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Mantenimiento</b></h2>
                        <input type="radio" name="mant_board" id="mant_board_si" value="1" {{ ($rol->mant_board=="1")? "checked" : "" }}>
                        <label for="mant_board_si">Si</label>
                        <input type="radio" name="mant_board" id="mant_board_no" value="0" {{ ($rol->mant_board=="0")? "checked" : "" }}>
                        <label for="mant_board_no">No</label>
                      </div>
                    </div>
                    <br>
                     <div class="row" align="center">
                      <div class="col-md-3">
                         <h2><b>Calidad</b></h2>
                        <input type="radio" name="cal_board" id="cal_board_si" value="1" {{ ($rol->cal_board=="1")? "checked" : "" }}>
                        <label for="cal_board_si">Si</label>
                        <input type="radio" name="cal_board" id="cal_board_no" value="0" {{ ($rol->cal_board=="0")? "checked" : "" }}>
                        <label for="cal_board_no">No</label>
                      </div>
                      <div class="col-md-3">
                         <h2><b>Servicio</b></h2>
                        <input type="radio" name="serv_board" id="serv_board_si" value="1" {{ ($rol->serv_board=="1")? "checked" : "" }}>
                        <label for="serv_board_si">Si</label>
                        <input type="radio" name="serv_board" id="serv_board_no" value="0" {{ ($rol->serv_board=="0")? "checked" : "" }}>
                        <label for="serv_board_no">No</label>
                      </div>
                       </div>
                  </div>
                  @endforeach
                  </table>

                </div>
                  @endif
              </div>
            </div>
            <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
              </div>
            </div>
        </div>

          </form>
      </div>
    </div>
  </div>
  @endsection