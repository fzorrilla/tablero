@extends('layouts.app', ['activePage' => 'board-management', 'titlePage' => __('Gestión de Tableros')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Código QR') }}</h4>
                <p class="card-category"> {{ __('') }}</p>
              </div>
              <div class="card-body">
                  <div class="row">
                  <!--<div class="col-md-1 text-right">
                      <a href="#" class="btn btn-sm btn-success">{{-- __('Descargar') --}}</a>
                  </div>-->
                  <div class="col-md-2 text-right">
                      <!--<a href="#" class="btn btn-sm btn-success">{{-- __('Imprimir') --}}</a>-->
                      <a href="javascript:window.print()" class="btn btn-sm btn-success">Imprimir</a>
                  </div>
                  <div class="col-md-9 text-right">
                      <a href="{{route('index_board')}}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                  </div>
                </div>
                   <center>  {!!QrCode::size(400)->generate($number) !!} </center>
                    <center><b><h2>{{$number}}</h2></b></center>


                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  @endsection