@extends('layouts.mob')

@section('title', 'Lima Lab Consulting Group')

@section('content')
<form class="form" method="POST" action="{{ route('login') }}">
@csrf
<div class="container container-mobile">
    <input type="hidden" name="id" value="{{$id}}">
    <div class="col-md-12 contain-img-icon">
      <div class="back-socorro col-md-6">

      </div>
    </div>

    <div class="col-md-12 pb-4">
        <label class="sr-only" for="usuario">Correo Electrónico</label>
        <div class="input-group mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text input-group-text-socorro"><i class="fa fa-envelope"></i></div>
          </div>
          <input type="text" class="form-control form-control-socorro" id="email" name="email" placeholder="Correo Electrónico" required>
        </div>
    </div>
    <div class="col-md-12 pb-3">
        <label class="sr-only" for="password">Contraseña</label>
        <div class="input-group mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text input-group-text-socorro"><i class="fa fa-lock"></i></div>
          </div>
          <input type="password" class="form-control form-control-socorro" id="password" name="password" placeholder="Contraseña" required>
        </div>
    </div>
    <div class="col-md-12 pt-1">
        <a href="{{ url('mobileRecoverPass') }}" class="link-socorro">
          ¿Olvidaste tu Contraseña?
        </a>
    </div>
    <div class="col-md-12 pt-3 pb-2">
          <input type="submit" value="Ingresar" class="socorro__card-button button-mobile-socorro" >
    </div>
</div>
</form>

@stop

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
	<script type="text/javascript">


	</script>
@stop
