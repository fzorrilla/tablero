@extends('layouts.mob')

@section('title', 'Lima Lab Consulting Group')

@section('content')

<div class="container container-mobile">

    <div class="col-md-12 contain-img-icon">
      <div class="back-socorro col-md-6">

      </div>
    </div>

      <div class="col-md-12 pb-3">
        <label class="sr-only" for="password">E-EMAIL</label>
        <div class="input-group mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text input-group-text-socorro"><i class="fa fa-envelope"></i></div>
          </div>
          <input type="text" class="form-control form-control-socorro" id="email" placeholder="E-MAIL">
        </div>
    </div>
    <!--<div class="col-md-12 pt-1">
        <a href="{{-- url('/mobile/recoveruser') --}}" class="link-socorro" >
          ¿Olvidaste tu Usuario?
        </a>
    </div>-->
    <div class="col-md-12 pt-3 pb-2">

        <a href="#">
          <input type="button" value="RECUPERAR CLAVE" class="socorro__card-button button-mobile-socorro" >
        </a>
    </div>
</div>
<div class="modal fade" id="recover-modal" tabindex="-1" role="dialog" aria-labelledby="edit-modal-label" aria-hidden="true">
  <div class="modal-dialog modal-lg content-modal-socorro-mobile" role="document">
    <div class="modal-content modal-content-socorro-mobile">
      <div class="modal-body" id="attachment-body-content">
        <div class="col-md-12 pt-3 pb-2 text-center">
          <img src="{{ asset('img/email.png') }}">
        </div>
        <div class="col-md-12 pt-3 pb-2 text-center">
          <h5 class="font-weight-bold">CORREO NO EXISTE EN NUESTRA BASE DE DATOS</h5>
        </div>
        <div class="col-md-12 pt-3 pb-2 text-center">
          <h5 class="font-weight-bold">CONTÁCTANOS A</h5>
          <p>socorro@socorro.pe</p>
        </div>
        <div class="col-md-12 pt-3 pb-2 text-center">
            <a href="javascript:void(0)">
              <input type="button" value="VOLVER" data-dismiss="modal" class="socorro__card-button button-mobile-socorro button-revover-close" >
            </a>
        </div>

      </div>
    </div>
  </div>
</div>

@stop

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
	<script type="text/javascript">
    $(document).on('click', "#emailold", function() {

      var options = {
        'backdrop': 'static'
      };
      $('#recover-modal').modal(options)
    })

	</script>
@stop
