@extends('layouts.mob')

@section('title', 'Lima Lab Consulting Group')

@section('content')

<div class="container container-mobile">

    <div class="container">
      <div class="row">
        <div class="col-md-8 pb-4 pr-0" style="width: 100%;text-align: left;">
          <div class="message-welcome-plan">
            Tablero :{{$number}}
            <br>
            Nombre :{{$name}}
          </div>
        </div>

      </div>
      <div class="col-md-8 pb-4 pr-0" style="width: 100%;text-align: left;">
          <div class="message-welcome-plan">
            <i class="fa fa-unlock" aria-hidden="true"> Público</i>
          </div>
        </div>
      <div class="row mt-1">
        <div class="col-md-6 pb-4" style="width: 25%">
          <a class="links-plains" href="/storage/{{$epps}}" target="blank">
            <img src="{{ asset('img/epps.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pl-0 pr-0">
              <center>EPPs</center>
            </div>
          </a>
        </div>
        <div class="col-md-6 pb-4" style="width: 25%">
          <a class="links-plains" href="/storage/{{$electric_system}}" target="blank">
            <img src="{{ asset('img/sistema_electrico.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pt-1 pl-0 pr-0">
              Sistema Eléctrico
            </div>
          </a>
        </div>
        <div class="col-md-6 pb-4" style="width: 25%">
          <a class="links-plains" href="/storage/{{$limit}}" target="blank">
            <img src="{{ asset('img/limite.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pt-1">
              Límites
            </div>
          </a>
        </div>
        <div class="col-md-6 pb-4" style="width: 25%">
          <a class="links-plains" href="/storage/{{$other}}" target="blank">
            <img src="{{ asset('img/other.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pt-1">
              Otro
            </div>
          </a>
        </div>
      </div>
      <div class="col-md-8 pb-4 pr-0" style="width: 100%;text-align: left;">
          <div class="message-welcome-plan">
            <i class="fa fa-lock" aria-hidden="true"> Privados</i>
          </div>
        </div>
      <div class="row mt-1">
        <div class="col-md-6 pb-4" style="width: 33%">
          <a class="links-plains" href="{{ url('mobileLogin',$id)}}">
            <img src="{{ asset('img/ingenieria.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pl-0 pr-0">
              Ingenieria
            </div>
          </a>
        </div>
        <div class="col-md-6 pb-4" style="width: 33%">
          <a class="links-plains" target="_blank" href="{{ url('mobileLogin',$id)}}">
            <img src="{{ asset('img/hse.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pt-1 pl-0 pr-0">
              HSE
            </div>
          </a>
        </div>
          <div class="col-md-6 pb-4" style="width: 33%">
          <a class="links-plains" href="{{ url('mobileLogin',$id)}}">
            <img src="{{ asset('img/calidad.png') }}" style="width: 72px;height: 72px;text-align: center;" >
            <div class="col-md-12 text-purple pt-1">
              Calidad
            </div>
          </a>
        </div>
        <div class="col-md-6 pb-4" style="width: 50%">
          <a class="links-plains" href="{{ url('mobileLogin',$id)}}">
            <img src="{{ asset('img/Mantenimiento.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pt-1 pl-0 pr-0">
              Mantenimiento
            </div>
          </a>
        </div>
        <div class="col-md-6 pb-4" style="width: 50%">
         <a class="links-plains" href="{{ url('mobileLogin',$id)}}">
            <img src="{{ asset('img/servicio.png') }}" style="width: 72px;height: 72px;text-align: center;">
            <div class="col-md-12 text-purple pt-1">
              Servicio
            </div>
          </a>
        </div>
      </div>

    </div>
</div>

@stop

@section('js')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
	<script type="text/javascript">


	</script>
@stop
