<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{ route('home') }}" class="simple-text logo-normal">
      {{ __('Mondelez International') }}
       <!--<img src="{{-- asset('assets/img/logo-dtel.png') --}}" alt="Logo">-->
    </a>
  </div>
  @if (Auth::user()->role_id == 1)
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user.index') }}">
          <span class="sidebar-mini"> <i class="material-icons">group</i> </span>
          <span class="sidebar-normal">{{ __('Usuarios y Permisos') }} </span>
        </a>
      </li>
       <li class="nav-item{{ $activePage == 'board' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('index_board') }}">
          <span class="sidebar-mini"> <i class="material-icons">ballot</i> </span>
          <span class="sidebar-normal">{{ __('Tableros') }} </span>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'report' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('report') }}">
          <span class="sidebar-mini"> <i class="material-icons">report</i> </span>
          <span class="sidebar-normal">{{ __('Reporte') }} </span>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('profile.edit') }}">
          <span class="sidebar-mini"> <i class="material-icons">perm_identity</i> </span>
          <span class="sidebar-normal">{{ __('Perfil de Usuario') }} </span>
        </a>
      </li>
    </ul>
  </div>
    @endif
  @if (Auth::user()->role_id == 2)
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user.index') }}">
          <span class="sidebar-mini"> <i class="material-icons">group</i> </span>
          <span class="sidebar-normal">{{ __('Usuarios y Permisos') }} </span>
        </a>
      </li>
       <li class="nav-item{{ $activePage == 'board' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('index_board') }}">
          <span class="sidebar-mini"> <i class="material-icons">ballot</i> </span>
          <span class="sidebar-normal">{{ __('Tableros') }} </span>
        </a>
      </li>
            <li class="nav-item{{ $activePage == 'report' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('report') }}">
          <span class="sidebar-mini"> <i class="material-icons">report</i> </span>
          <span class="sidebar-normal">{{ __('Reporte') }} </span>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('profile.edit') }}">
          <span class="sidebar-mini"> <i class="material-icons">perm_identity</i> </span>
          <span class="sidebar-normal">{{ __('Perfil de Usuario') }} </span>
        </a>
      </li>
    </ul>
  </div>
    @endif
     @if (Auth::user()->role_id == 3)
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'board' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('index_board') }}">
          <span class="sidebar-mini"> <i class="material-icons">ballot</i> </span>
          <span class="sidebar-normal">{{ __('Tableros') }} </span>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('profile.edit') }}">
          <span class="sidebar-mini"> <i class="material-icons">perm_identity</i> </span>
          <span class="sidebar-normal">{{ __('Perfil de Usuario') }} </span>
        </a>
      </li>
    </ul>
  </div>
    @endif
</div>