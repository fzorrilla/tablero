@extends('layouts.app', ['activePage' => 'archive', 'titlePage' => __('Gestión de Tableros')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Archivos Públicos') }}</h4>
                <p class="card-category"> {{ __('Aquí puedes administrar los archivos públicos') }}</p>
                <input type="search" id="search"  placeholder="Buscar" title="Presione Esc para supender la búsqueda">
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">Cerrar</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                 @if (Auth::user()->role_id == 1)
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{route('edit_archive')}}" class="btn btn-sm btn-primary">{{ __('Actualizar Archivos') }}</a>
                  </div>
                </div>
                @endif
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                         <center>{{ __('Epps') }}</center>
                      </th>
                       <th>
                           <center>{{ __('Sistema Electrico') }}</center>
                      </th>
                      <th>
                         <center>{{ __('Limites') }}</center>
                      </th>
                       <th>
                         <center>{{ __('Otro') }}</center>
                      </th>
                    </thead>
                    <tbody >
                   @foreach($archives as $archive)
                      <tr>
                          <td>
                         @if( $archive->epps <> '')
                        <center> <a href="#" value="{{  $archive->epps}}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a></center>
                        @else
                        <center> <img src="{{ asset('material') }}/img/error.png" width="50" height="50"></center>
                        @endif
                      </td>
                      <td>
                          @if( $archive->electric_system <> '')
                        <center> <a href="#" value="{{$archive->electric_system}}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a></center>
                        @else
                         <center><img src="{{ asset('material') }}/img/error.png" width="50" height="50"></center>
                        @endif
                      </td>
                       <td>
                        @if( $archive->limit <> '')
                         <center><a href="#" value="{{  $archive->limit}}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a></center>
                        @else
                        <center> <img src="{{ asset('material') }}/img/error.png" width="50" height="50"></center>
                        @endif
                      </td>
                       <td>
                         @if( $archive->other <> '')
                        <center> <a href="#" value="{{  $archive->limit}}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a></center>
                        @else
                         <center><img src="{{ asset('material') }}/img/error.png" width="50" height="50"></center>
                        @endif
                      </td>
                        </tr>

                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
  <script>
  $(document).ready(function(){
    $("#search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#table tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
</script>
