@extends('layouts.app', ['activePage' => 'board', 'titlePage' => __('Gestión de Tableros')])
 <script type="text/javascript">
    (function(document) {
      'use strict';

      var LightTableFilter = (function(Arr) {

        var _input;

        function _onInputEvent(e) {
          _input = e.target;
          var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
          Arr.forEach.call(tables, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
              Arr.forEach.call(tbody.rows, _filter);
            });
          });
        }

        function _filter(row) {
          var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            Arr.forEach.call(inputs, function(input) {
              input.oninput = _onInputEvent;
            });
          }
        };
      })(Array.prototype);

      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          LightTableFilter.init();
        }
      });

    })(document);
    </script>
@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Tableros') }}</h4>
                <p class="card-category"> {{ __('Aquí puedes administrar los tableros') }}</p>
              </div>
              <br>
               <div class="row">
                  <div class="col-12 text-right">
                    <input class="form-control col-md-3 light-table-filter" style="display: inline;" data-table="order-table" type="text" placeholder="Buscar" title="Presione Esc para supender la búsqueda">
                  </div>
                </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">Cerrar</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                 @if (Auth::user()->role_id == 1)
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{route('create_board')}}" class="btn btn-sm btn-primary">{{ __('Agregar Tablero') }}</a>
                  </div>
                </div>
                @endif
                <div class="table-responsive">
                  <table class="table order-table">
                    <thead class=" text-primary">
                      <th class="text-center">
                         <b> {{ __('Número') }}</b>
                      </th>
                      <th class="text-center">
                         <b> {{ __('Nombre') }}</b>
                      </th>
                      <th class="text-center">
                        <b>{{ __('Código QR') }}</b>
                      </th>
                      @if (Auth::user()->role_id == 1)
                      <th class="text-left">
                        {{ __('') }}
                      </th>
                       <th class="text-left">
                       <b> {{ __('Acción') }}</b>
                      </th>
                      <th class="text-center">
                        <b>{{ __('Públicos') }}</b>
                      </th>
                      <th class="text-center">
                        <b>{{ __('Privada') }}</b>
                      </th>
                      @endif
                      @if (Auth::user()->role_id == 2)
                       <th class="text-center">
                       <b> {{ __('Privado') }}</b>
                      </th>
                      @endif
                      @if (Auth::user()->role_id == 3)
                       <th class="text-center">
                        <b>{{ __('Privado') }}</b>
                      </th>
                      @endif

                    </thead>
                    <tbody id="tablero">
                   @foreach($boards as $board)
                      <tr>
                      <td class="text-center">
                         {{ $board->number }}
                      </td>
                      <td class="text-center">
                          {{ $board->name }}
                      </td>
                      <td class="text-center">
                        <a href="{{ route('qr_board', $board->id) }}"> <img src="{{ asset('material') }}/img/qr.png" width="60" height="60">
                          </a>
                      </td>
                    @if (Auth::user()->role_id == 1)
                      <td class="td-actions text-center">
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('edit_board', $board->id) }}" data-original-title="">
                                    <i class="material-icons">edit</i>

                                  </a>
                      </td>
                      <td>
                                <form action="{{ route('destroy_board', $board->id) }}" method="post">
                                  @csrf
                                  @method('delete')
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="Eliminar Tablero" onclick="confirm('{{ __("Estás seguro de que deseas eliminar a este usuario?") }}') ? this.parentElement.submit() : ''">
                                      <i class="material-icons">delete</i>
                                      <div class="ripple-container"></div>
                                  </button>
                              </form>
                          </td>
                             <td class="td-actions text-center">

                                  <a rel="tooltip" class="btn btn-primary btn-link" href="{{ route('edit_board_public', $board->id) }}"data-original-title="">
                                    <i class="material-icons">cloud_done</i>
                                    <div class="ripple-container"></div>
                                  </a>
                      </td>
                           <td class="td-actions text-center">

                                  <a rel="tooltip" class="btn btn-danger btn-link" href="{{ route('edit_board', $board->id) }}" data-original-title="" >
                                    <i class="material-icons">backup</i>
                                    <div class="ripple-container"></div>
                                  </a>
                      </td>
                          @endif
                           @if (Auth::user()->role_id == 2)
                            <td class="td-actions text-center">

                                        <a rel="tooltip" class="btn btn-danger btn-link" href="{{ route('edit_board', $board->id) }}"data-original-title="">
                                          <i class="material-icons">backup</i>
                                          <div class="ripple-container"></div>
                                        </a>
                            </td>
                          @endif
                            @if (Auth::user()->role_id == 3)
                            <td class="td-actions text-center">

                                        <a rel="tooltip" class="btn btn-danger btn-link" href="{{ route('edit_superv_board', [Auth::user()->id,$board->id]) }}"data-original-title="" >
                                          <i class="material-icons">backup</i>
                                          <div class="ripple-container"></div>
                                        </a>
                            </td>
                          @endif
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection


