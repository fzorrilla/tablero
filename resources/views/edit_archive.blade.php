@extends('layouts.app', ['activePage' => 'archive', 'titlePage' => __('Gestión de Tableros')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{route('update_archive','1')}}" autocomplete="off" class="form-horizontal">
            @csrf
             @method('put')
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Archivos Públicos') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                 @if (Auth::user()->role_id == 1)
                 <input type="hidden" name="role_id" value="{{Auth::user()->role_id}}">
                 <input type="hidden" name="id" value="1">
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                     <option value="epps">EPPs</option>
                     <option value="electric_system">Sistema electricos</option>
                     <option value="limit">Limites</option>
                     <option value="other">Otro</option>
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                      <input id="file_input_file" class="none" type="file" name="archive" />
                    </label>
                  </div>
              </div><!--fin-->
              @endif


              </div><!--fin-->
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Actualizar Archivo') }}</button>
              </div>
            </div>

          </form>

      </div>
    </div>
  </div>
@endsection