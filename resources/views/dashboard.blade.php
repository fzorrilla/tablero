@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="material-icons">group</i>
              </div>
              <p class="card-category">Usuarios</p>
              <h3 class="card-title">{{$user}}
                <?php
                $users = $user;
                $user_sa=$user_total_sa;
                $user_a=$user_total_a;
                $user_s=$user_total_s;
                 ?>
              </h3>
            </div>
             <div class="card-footer">
              <div class="stats">
                <i class="material-icons"></i> Cantidad de Usuarios
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons">ballot</i>
              </div>
              <p class="card-category">Tableros</p>
              <h3 class="card-title">{{$board}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons"></i> Cantidad de Tableros
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="material-icons">unarchive</i>
              </div>
              <p class="card-category">Archivos</p>
              <h3 class="card-title">{{$archive}}</h3>
               <?php
                $archive_on = $archive;
                 ?>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons"></i> Cantidad Archivos Subidos
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
               <i class="material-icons">archive</i>
              </div>
              <p class="card-category">Archivos</p>
              <h3 class="card-title">{{$archive_off}}</h3>
               <?php
                $archive_off = $archive_off;
                 ?>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons"></i> Cantidad de Archivo Faltantes</div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
       <!-- <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-success">
              <div class="ct-chart" id="dailySalesChart"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Cantidad de Usuario-Rol</h4>
              <p class="card-category">
                <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">access_time</i> updated 4 minutes ago
              </div>
            </div>
          </div>
        </div>-->
         <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-warning">
              <div class="ct-chart" id="websiteViewsChart1"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Usuarios</h4>
              <p class="card-category">SuperAdmin, Admin y Supervisores</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-success">
              <div class="ct-chart" id="websiteViewsChart"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Archivos</h4>
              <p class="card-category">Subidos- No Subidos</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons"></i>
              </div>
            </div>
          </div>
        </div>
       <!-- <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-danger">
              <div class="ct-chart" id="completedTasksChart"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Completed Tasks</h4>
              <p class="card-category">Last Campaign Performance</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">access_time</i> campaign sent 2 days ago
              </div>
            </div>
          </div>
        </div>
      </div>-->

    </div>
  </div>
@endsection

@push('js')
  <script>
   /* $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });*/
       var user = '<?php echo $users;?>';
      var archive_on = '<?php echo $archive_on;?>';
      var archive_off = '<?php echo $archive_off;?>';
      var user_sa = '<?php echo $user_sa;?>';
      var user_a = '<?php echo $user_a;?>';
      var user_s = '<?php echo $user_s;?>';
     console.log(user);
     console.log(archive_on);
     console.log(archive_off);
     console.log(user_sa);
     console.log(user_a);
     console.log(user_s);

    if ($('#websiteViewsChart1').length != 0 || $('#completedTasksChart').length != 0 || $('#websiteViewsChart').length != 0) {

      var dataWebsiteViewsChart = {
       labels: ['Subido', 'No Subido'],
       series: [
         [archive_on, archive_off]
        ]
      };
      var optionsWebsiteViewsChart = {
        axisX: {
          showGrid: false
        },
        low: 0,
        high: 50,
        chartPadding: {
          top: 0,
          right: 5,
          bottom: 0,
          left: 0
        }
      };
      var responsiveOptions = [
        ['screen and (max-width: 40px)', {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function(value) {
              return value[0];
            }
          }
        }]
      ];
      var websiteViewsChart = Chartist.Bar('#websiteViewsChart', dataWebsiteViewsChart, optionsWebsiteViewsChart, responsiveOptions);

      //start animation for the Emails Subscription Chart
      md.startAnimationForBarChart(websiteViewsChart);
    }

    var dataWebsiteViewsChart1 = {
        labels: ['SAdmin', 'Admin', 'Supervisor'],
        series: [
          [user_sa, user_a,user_s]

        ]
      };
      var optionsWebsiteViewsChart1 = {
        axisX: {
          showGrid: false
        },
        low: 0,
        high: 50,
        chartPadding: {
          top: 0,
          right: 5,
          bottom: 0,
          left: 0
        }
      };
      var responsiveOptions = [
        ['screen and (max-width: 740px)', {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function(value) {
              return value[0];
            }
          }
        }]
      ];
      var websiteViewsChart1 = Chartist.Bar('#websiteViewsChart1', dataWebsiteViewsChart1, optionsWebsiteViewsChart1, responsiveOptions);

      //start animation for the Emails Subscription Chart
      md.startAnimationForBarChart(websiteViewsChart1);


  </script>
@endpush