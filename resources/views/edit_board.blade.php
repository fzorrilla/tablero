@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Gestión de Tableros')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('board.update', $id) }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Agregar Tablero Privados') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{route('index_board')}}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                  </div>
                </div>
                 @if (Auth::user()->role_id == 1)
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <input type="hidden" name="number" value="{{$number}}">
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number', $number) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name', $name) }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                     <option value="hse">HSE</option>
                     <option value="mant">Mantenimiento</option>
                     <option value="ing">Ingenieria</option>
                     <option value="cal">Calidad</option>
                     <option value="serv">Servicio</option>
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                      <input id="file" class="none" type="file" name="archive" />
                    </label>
                  </div>
                              <div class="row">
                  <div class="col-sm-12">
                    <input type="radio" name="status" id="publico" value="1">
                          <label for="publico">Público</label>
                    <input type="radio" name="status" id="privado" value="0" checked="">
                          <label for="privado">Privado</label>
                  </div>
                </div>
              </div><!--fin-->
              @endif
                @if (Auth::user()->role_id == 2)
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <input type="hidden" name="number" value="{{$number}}">
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number', $number) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <input type="hidden" name="name" value="{{$name}}">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name', $name) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                     <option value="hse">HSE</option>
                     <option value="mant">Mantenimiento</option>
                     <option value="ing">Ingenieria</option>
                     <option value="cal">Calidad</option>
                     <option value="serv">Servicio</option>
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                       <input id="file" class="none" type="file" name="archive" />
                    </label>
                  </div>
                         <div class="row">
                  <div class="col-sm-12">
                    <input type="radio" name="status" id="publico" value="1">
                          <label for="publico">Público</label>
                    <input type="radio" name="status" id="privado" value="0" checked="">
                          <label for="privado">Privado</label>
                  </div>
                </div>
              </div><!--fin-->
              @endif
               @if (Auth::user()->role_id == 3)
               <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
               <input type="hidden" name="role_id" value="{{Auth::user()->role_id}}">
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <input type="hidden" name="number" value="{{$number}}">
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number',$number) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <input type="hidden" name="name" value="{{$name}}">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name',$name ) }}" required="true" aria-required="true" disabled/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                 <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                    @if($hse == 1)
                     <option value="hse">HSE</option>
                    @endif
                    @if($mant == 1)
                     <option value="mant">Mantenimiento</option>
                    @endif
                    @if($ing == 1)
                     <option value="ing">Ingenieria</option>
                    @endif
                    @if($cal == 1)
                     <option value="cal">Calidad</option>
                    @endif
                    @if($serv == 1)
                     <option value="serv">Servicio</option>
                     @endif
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                      <input id="file" class="none" type="file" name="archive" />
                    </label>
                  </div>
                         <div class="row">
                  <div class="col-sm-12">
                    <input type="radio" name="status" id="publico" value="1">
                          <label for="publico">Público</label>
                    <input type="radio" name="status" id="privado" value="0" checked="">
                          <label for="privado">Privado</label>
                  </div>
                </div>
              </div><!--fin-->
              @endif

              </div><!--fin-->
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Actualizar Tablero') }}</button>
              </div>
            </div>

          </form>
          <div class="card-header card-header-primary ">
              <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                          <center><b>{{ __('Archivo') }}</b></center>
                      </th>
                       <th>
                          <b>{{ __('Adjunto') }}</b>
                      </th>
                    </thead>
                    <tbody>
                       @foreach($boards as $board)
                      <tr>
                      <td>
                         <center>HSE</center>
                      </td>
                      <td>
                        @if($board->hse <> '')
                         <a href="#" value="{{ $board->hse }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                     <tr>
                      <td>
                         <center>Manteniemiento</center>
                      </td>
                      <td >
                         @if($board->mant <> '')
                         <a href="#" value="{{ $board->mant }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>

                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                      <tr>
                        <tr>
                       <td>
                         <center>Ingenieria</center>
                      </td>
                      <td>
                         @if($board->ing <> '')
                         <a href="#" value="{{ $board->ing }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                     <tr>
                       <td>
                         <center>Calidad</center>
                      </td>
                      <td>
                         @if($board->cal <> '')
                         <a href="#" value="{{ $board->cal }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                    <tr>
                       <td>
                         <center>Servicio</center>
                      </td>
                      <td>
                         @if($board->serv <> '')
                         <a href="#" value="{{ $board->serv }}"> <img src="{{ asset('material') }}/img/pdf.jpg" width="50" height="50">
                          </a>
                        @else
                         <img src="{{ asset('material') }}/img/error.png" width="50" height="50">
                        @endif
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection