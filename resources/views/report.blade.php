@extends('layouts.app', ['activePage' => 'report', 'titlePage' => __('Reporte')])
 <script type="text/javascript">
    (function(document) {
      'use strict';

      var LightTableFilter = (function(Arr) {

        var _input;

        function _onInputEvent(e) {
          _input = e.target;
          var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
          Arr.forEach.call(tables, function(table) {
            Arr.forEach.call(table.tBodies, function(tbody) {
              Arr.forEach.call(tbody.rows, _filter);
            });
          });
        }

        function _filter(row) {
          var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
          row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
          init: function() {
            var inputs = document.getElementsByClassName('light-table-filter');
            Arr.forEach.call(inputs, function(input) {
              input.oninput = _onInputEvent;
            });
          }
        };
      })(Array.prototype);

      document.addEventListener('readystatechange', function() {
        if (document.readyState === 'complete') {
          LightTableFilter.init();
        }
      });

    })(document);
    </script>
@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Reporte General Archivos</h4>
            <p class="card-category">Privados</p>
          </div>
          <br>
          <div class="row">
                  <div class="col-12 text-right">
                    <input class="form-control col-md-3 light-table-filter" style="display: inline;" data-table="order-table" type="text" placeholder="Buscar" title="Presione Esc para supender la búsqueda">
                  </div>
                </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table order-table">
                <thead class=" text-primary">
                  <th>
                   <center> Tablero</center>
                  </th>
                  <th>
                    <center> Nombre</center>
                  </th>
                  <th>
                    <center>EPPS</center>
                  </th>
                  <th>
                    <center>S.Eléctrico</center>
                  </th>
                  <th>
                    <center>Limites</center>
                  </th>
                  <th>
                    <center>Otro</center>
                  </th>
                  <th>
                    <center>HSE</center>
                  </th>
                  <th>
                    <center>Ing</center>
                  </th>
                   <th>
                    <center>Mant</center>
                  </th>
                  <th>
                    <center>Calidad</center>
                  </th>
                  <th>
                    <center>Servicio</center>
                  </th>
                </thead>
                <tbody>
                   @foreach($boards as $board)
                      <tr>
                      <td>
                         <center>{{ $board->number }}</center>
                      </td>
                      <td>
                         <center> {{ $board->name }}</center>
                      </td>
                      <td>
                        @if($board->epps <> '')
                          <a href="/storage/{{$board->epps}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board_public', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->electric_system <> '')
                        <a href="/storage/{{$board->electric_system}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                        <a href="{{ route('edit_board_public', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->limit <> '')
                         <a href="/storage/{{$board->limit}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board_public', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->other <> '')
                         <a href="/storage/{{$board->other}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board_public', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->hse <> '')
                         <a href="/storage/{{$board->hse}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->ing <> '')
                         <a href="/storage/{{$board->ing}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->mant <> '')
                          <a href="/storage/{{$board->mant}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->cal <> '')
                          <a href="/storage/{{$board->cal}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                      <td>
                        @if($board->serv <> '')
                          <a href="/storage/{{$board->serv}}" target="blank"><center> <img src="{{ asset('material') }}/img/comprobar.png" width="30" height="30"></center></a>
                        @else
                         <a href="{{ route('edit_board', $board->id) }}"><center><img src="{{ asset('material') }}/img/error.png" width="30" height="30"></center></a>
                        @endif
                      </td>
                    </tr>
                    @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      </div>
    </div>
  </div>
</div>
@endsection