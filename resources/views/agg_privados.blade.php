@extends('layouts.app', ['activePage' => 'user-management', 'titlePage' => __('Gestión de Tableros')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{route('board.store')}}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Agregar Tablero Privados') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{route('index_board')}}" class="btn btn-sm btn-primary">{{ __('Volver a la lista') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Numero') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('number') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" id="input-name" type="number" placeholder="{{ __('Numero') }}" value="{{ old('number') }}" required="true" aria-required="true"  />
                      @if ($errors->has('number'))
                        <span id="number-error" class="error text-danger" for="input-name">{{ $errors->first('number') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                   <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Nombre') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="{{ old('name') }}" required="true" aria-required="true"/>
                      @if ($errors->has('name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                <label class="col-sm-2 col-form-label" for="">{{ __('Archivo de Subida') }}</label>
                <div class="col-md-4">
                    <div class="form-group">
                      <select class="form-control" id="type_area" name="type_area">
                         <option value="">
                      Seleccione
                    </option>
                     <option value="hse">HSE</option>
                     <option value="mant">Mantenimiento</option>
                     <option value="ing">Ingenieria</option>
                     <option value="cal">Calidad</option>
                     <option value="serv">Servicio</option>
                  </select>
                    </div>
                  </div>

                 <div class="file_input col-md-4">
                    <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
                     <!-- <i class="material-icons">file_upload</i>-->
                      <input id="file_input_file" class="none" type="file" name="archive" />
                    </label>
                  </div>
              </div><!--fin-->
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Agregar Usuario') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection


