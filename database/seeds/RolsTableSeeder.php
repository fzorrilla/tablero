<?php

use Illuminate\Database\Seeder;

class RolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rols = [
            [
                'name' => 'SuperAdministrador',
                'slug' => 'SuperAdmin',
                'description' => 'SuperAdministrador',
                'level' => '5',
            ],
             [
                'name' => 'Administrador',
                'slug' => 'Admin',
                'description' => 'Administrador',
                'level' => '3',
            ],
             [
                'name' => 'Supervisor',
                'slug' => 'Supervisor',
                'description' => 'Supervisor',
                'level' => '1',
            ],
        ];


        foreach ($rols as $rol) {
            App\Rol::create($rol);
        }
    }
}

