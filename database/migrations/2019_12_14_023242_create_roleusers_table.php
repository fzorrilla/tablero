<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoleusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roleusers', function (Blueprint $table) {
            $table->bigIncrements('id');
           $table->unsignedBigInteger ('role_id');
                $table->foreign('role_id')
                ->references('id')
                ->on('rols')
                ->onDelete('cascade');
                 $table->unsignedBigInteger ('user_id');
                 $table->foreign('user_id')
                 ->references('id')
                 ->on('users')
                 ->onDelete('cascade');
                    $table->string('create_user');
                    $table->string('edit_user');
                    $table->string('delete_user');
                    $table->string('permission_user');
                    $table->string('create_board');
                    $table->string('edit_board');
                    $table->string('delete_board');
                    $table->string('permission_board');
                    $table->string('bulk_load');
                    $table->string('hse_board');
                    $table->string('ing_board');
                    $table->string('mant_board');
                    $table->string('cal_board');
                    $table->string('serv_board');
                    $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roleusers');
    }
}
