<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number');
            $table->string('name');
            $table->string('epps')->nullable();
            $table->string('epps_status')->nullable();
            $table->string('electric_system')->nullable();
            $table->string('electric_system_status')->nullable();
            $table->string('limit')->nullable();
            $table->string('limit_status')->nullable();
            $table->string('other')->nullable();
            $table->string('other_status')->nullable();
            $table->string('hse')->nullable();
            $table->string('hse_status')->nullable();
            $table->string('mant')->nullable();
            $table->string('mant_status')->nullable();
            $table->string('ing')->nullable();
            $table->string('ing_status')->nullable();
            $table->string('cal')->nullable();
            $table->string('cal_status')->nullable();
            $table->string('serv')->nullable();
            $table->string('serv_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boards');
    }
}
